#ifndef VECTOR2_H
#define VECTOR2_H

#include "global.h"

template<class Scalar>
class Vector2
{
    Scalar x;
    Scalar y;

public:
    Vector2 (Scalar _x = 0.0f, Scalar _y = 0.0f):x(_x), y(_y)
    {}

    Vector2 (const Vector2<Scalar> &in):x(in.x), y(in.y)
    {}

    ~Vector2 (){}

    inline Scalar getX() const;
    inline void setX(Scalar _x);

    inline Scalar getY() const;
    inline Scalar setY(Scalar _y) const;

    inline const Vector2<Scalar> & operator= (const Vector2<Scalar> &_v);
    inline bool operator== (const Vector2<Scalar> &_v) const;

    inline const Vector2<Scalar> operator+ (const Vector2<Scalar> &_v) const;
    inline const Vector2<Scalar> operator- (const Vector2<Scalar> &_v) const;
    inline Scalar operator*(const Vector2<Scalar> &_v) const;

    template <class S>
    friend inline const Vector2<S> operator -(const Vector2<S> &_v);

    template <class S>
    friend inline Vector2<S> operator*(S, const Vector2<S> &_v);

    template <class S>
    friend inline Vector2<S> operator*(const Vector2<S> &_v, S);

    inline Scalar length() const;
    inline Scalar lengthSquare() const;

    inline Scalar angle() const;

    inline Vector2<Scalar> getPerpendicular() const;
    inline Vector2<Scalar> getInversePerpendicular() const;

    inline Vector2<Scalar> getProjection(const Vector2<Scalar> v) const;

    inline void clampToLength();
    inline Vector2<Scalar> getClampedToLength(Scalar len) const;

    inline void normalize();
    inline Vector2<Scalar> getNormalized() const;

/*
    static inline cpVect cpvlerpconst(cpVect v1, cpVect v2, cpFloat d)
    {
        return cpvadd(v1, cpvclamp(cpvsub(v2, v1), d));
    }
*/
    inline Scalar distance(const Vector2<Scalar> v1, const Vector2<Scalar> v2) const;
    inline bool isWithinDistance(const Vector2<Scalar> v1, const Vector2<Scalar> v2, const Scalar dist) const;

    inline static Vector2<Scalar> unitVectorOfAngle(Scalar angle);
    inline static Vector2<Scalar> sphericalLerp(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2);
    inline static Vector2<Scalar> sphericalLerpNoMoreThanAngle(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2);
    inline static Vector2<Scalar> lerp(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2);
    inline static Vector2<Scalar> multi(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2);
    inline static Scalar dot(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2);
    inline static Vector2<Scalar> cross(const Vector2<Scalar> &v1,const Vector2<Scalar> &v2);
};

typedef Vector2<double> Vector2d;
typedef Vector2<float> Vector2f;

#ifdef DOUBLE_SCALAR
typedef double Scalar;
typedef Vector2d Vector;
#else
typedef float Scalar;
typedef Vector2f Vector;
#endif

#endif // VECTOR2_H
