#include "vector2.h"
#include <cmath>

template <class Scalar>
inline Scalar Vector2<Scalar>::getX() const
{
    return x;
}

template <class Scalar>
inline void Vector2<Scalar>::setX(Scalar _x)
{
    x = _x;
}

template <class Scalar>
inline Scalar Vector2<Scalar>::getY() const
{
    return y;
}

template <class Scalar>
inline Scalar Vector2<Scalar>::setY(Scalar _y) const
{
    y = _y;
}

template <class Scalar>
inline const Vector2<Scalar> & Vector2<Scalar>::operator= (const Vector2<Scalar> &_v)
{
    x = _v.x;
    y = _v.y;

    return this;
}

template <class Scalar>
inline bool Vector2<Scalar>::operator== (const Vector2<Scalar> &_v) const
{
    return (x == _v.x && y == _v.y);
}

template <class Scalar>
inline const Vector2<Scalar> Vector2<Scalar>::operator+ (const Vector2<Scalar> &_v) const
{
    Vector2<Scalar> result(x+_v.x,y+_v.y);
    return result;
}

template <class Scalar>
inline const Vector2<Scalar> Vector2<Scalar>::operator- (const Vector2<Scalar> &_v) const
{
    Vector2<Scalar> result(x-_v.x,y-_v.y);
    return result;
}

template <class Scalar>
inline Scalar Vector2<Scalar>::operator*(const Vector2<Scalar> &_v) const
{
    return x*_v.x+y*_v.y;
}

template <class Scalar>
inline Scalar Vector2<Scalar>::length() const
{
    return sqrt(x*x + y*y);
}

template <class Scalar>
inline Scalar Vector2<Scalar>::lengthSquare() const
{
    return x*x + y*y;
}

template <class Scalar>
inline Scalar Vector2<Scalar>::angle() const
{
}

template <class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::getPerpendicular() const
{
    Vector2<Scalar> result(-y,x);
    return result;
}

template <class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::getInversePerpendicular() const
{
    Vector2<Scalar> result(y,-x);
    return result;
}

template <class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::getProjection(const Vector2<Scalar> v) const
{
    Vector2<Scalar> result = (dot(v, *this) / dot(*this, *this)) * (*this);
    return result;
}

template <class Scalar>
inline void Vector2<Scalar>::clampToLength(Scalar len)
{
    return dot(*this,*this) > len * len ?
}

template <class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::getClampedToLength() const
{
}

template <class Scalar>
inline void Vector2<Scalar>::normalize()
{
    Scalar len = length();
    if(len != 0.0f)
    {
        Scalar lenInverse = (Scalar) 1.0 / len;
        x*=lenInverse;
        y*=lenInverse;
    }
}

template <class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::getNormalized() const
{
    Vector2<Scalar> result = *this;
    return result.normalize();
}

/*
static inline cpVect cpvlerpconst(cpVect v1, cpVect v2, cpFloat d)
{
    return cpvadd(v1, cpvclamp(cpvsub(v2, v1), d));
}
*/

template <class Scalar>
inline Scalar Vector2<Scalar>::distance(const Vector2<Scalar> v1, const Vector2<Scalar> v2) const
{
    return (v1- v2).length();
}

template <class Scalar>
inline bool Vector2<Scalar>::isWithinDistance(const Vector2<Scalar> v1, const Vector2<Scalar> v2, const Scalar dist) const
{
    return distanceSquare(v1, v2) < dist*dist;
}

template<class Scalar>
inline const Vector2<Scalar> operator-(const Vector2<Scalar> &_v)
{
    Vector2<Scalar> result(-_v.x, _v.y);
    return result;
}

template<class Scalar>
inline Vector2<Scalar> operator*(Scalar s, const Vector2<Scalar> &_v)
{
    Vector2<Scalar> result(s*_v.x, s*_v.y);
    return result;
}

template<class Scalar>
inline Vector2<Scalar> operator*(const Vector2<Scalar> &_v, Scalar s)
{
    Vector2<Scalar> result(s*_v.x,s*_v.y);
    return result;
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::unitVectorOfAngle(Scalar angle)
{
    Vector2<Scalar> result(cos(angle), sin(angle));
    return result;
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::sphericalLerp(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2)
{
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::sphericalLerpNoMoreThanAngle(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2)
{
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::lerp(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2)
{
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::multi(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2)
{
}

template<class Scalar>
inline Scalar Vector2<Scalar>::dot(const Vector2<Scalar> &v1, const Vector2<Scalar> &v2)
{
}

template<class Scalar>
inline Vector2<Scalar> Vector2<Scalar>::cross(const Vector2<Scalar> &v1,const Vector2<Scalar> &v2)
{
}
