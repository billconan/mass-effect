#-------------------------------------------------
#
# Project created by QtCreator 2012-02-25T22:07:05
#
#-------------------------------------------------

QT       += core gui

TARGET = MassEffect
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    vector2.cpp

HEADERS  += mainwindow.h \
    vector2.h \
    global.h

